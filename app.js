// Estoy importando el paquete de colors
require('colors'); // cuando es solo require sin ninguna variable es decirle que sea de manera global
// traemos la libreria yargs
const argv = require('yargs').argv;

const { generarArchivo } = require('./helpers/multiplicar');


/*console.log(process.argv);
console.log(argv); */

// con esta sacamos el valor de la variable
// console.log('base: yargs', argv.base);

// const base = 7;

// argv sirve para pasar variables
// console.log(process.argv);

// en process.argv se le pueden mandar los parámetros que quieras y lo almacena, y lo debes desestructurar
/* const [, , arg3 = 'base=9'] = process.argv;
// con split va a separar a los elementos por el signo igual '='. Debes separarlo con un split a cada parámetro que le pasas.
// El --base se queda detrás de la coma, y el valor de 5 aparte, son separados por el split por un '='
const [, base] = arg3.split('=');
console.log(arg3);
console.log(base);
*/

// es una promesa porque en el helpers/multiplicar se usa un async
generarArchivo(argv.base)
  .then(nombreArchivo => console.log(nombreArchivo.rainbow, 'creado'))
  .catch(error => console.log(error))



