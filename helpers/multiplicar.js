// importando el paquete fs (file system)
const fs = require('fs');
// Importando paquete colors
require('colors');

// El parámetro por defecto es 5
const crearArchivo = async (base = 5) => {
  try {
    let salida = '';
    console.log('====================='. green);
    console.log(`    Tabla del ${base}      `.green);
    console.log('====================='.green);

    for (let i = 0; i <= 10; i++) {
      salida += `${base} x ${i} = ${base * i}\n`.cyan;      
    }

    console.log(salida);
    

  /* 
    - 1er parámetro.- Que nombre va a tener tu archivo
    - 2do parámetro.- Que contenido va a tener tu archivo
    - 3er parámetro.- Es el error que puede salir
  */

 /*  fs.writeFile(`tabla-${base}.txt`, salida, (err) => {
    // si hay un error se detiene el programa y ya no pasa al log
    if (err) {
      throw err
    }
    console.log(`table-${base} creado`);
  }) */


  // Esta es una versión mejorada de fs.writeFile  
  fs.writeFileSync(`tabla-${base}.txt`, salida);
    //  console.log(`tabla-${base}.txt creado`);
    return `tabla-${base}.txt`;
   }
   catch (error) {
     throw error;
   }
};

//Exportamos el archivo
module.exports = {
  generarArchivo: crearArchivo
}
